# Pouruoi Angular ?

Comme tous les frameworks front end, Angular permet au développeur d’apporter une architecture, ce qui apporte une meilleure lisibilité et maintenabilité du code.
Certaines fonctionnalités communes dans le développement web ont déjà des solutions optimales, qu'elles soient basiques ou avancées, et recoder ces fonctionnalités peut être inefficace et conduire à des résultats moins performants. Il est donc recommandé d'utiliser des bibliothèques et des frameworks établis pour bénéficier de solutions éprouvées et optimisées.

Les projets Angular ont généralement une structure de code similaire et utilisent TypeScript, ce qui rend le code plus facile à lire et à entretenir. Angular est développé par Google, il intègre de nombreuses bibliothèques maintenues par celui-ci, ce qui le rend plus fiable.

Cette façon de structurer les applications dans Angular rend la maintenance, l'évolution et les tests plus simples en séparant clairement les responsabilités et en permettant la réutilisation du code. De plus, Angular propose des fonctionnalités comme le routage et les modules pour organiser efficacement les applications de grande taille. Ce qui facilite la gestion de la sécurité de l'application. 
Angular offre des fonctionnalités pour gérer efficacement les données sensibles, telles que le chiffrement côté client et la protection contre les attaques CSRF (Cross-Site Request Forgery). De plus, l'architecture permet de restreindre l'accès aux données sensibles uniquement aux parties nécessaires de l'application, en plus de la facilité qu’apporte ce framework pour la maintenabilité du code.

# Initialiser son premier projet Angular

## Installer Angular

Vérifier les versions de npm et de nvm avec les commandes `nvm -v` et `npm -v`.
S'assurer que la verssion npm est compatible avec la dernière version d'angular sur la doc officielle  https://angular.io/guide/versions, autrement installer la bonne version et utiliser la commande `nvm use (numéro-de-version)` pour l'utiliser.

Utiliser la commande `npm install -g @angular/cli` pour installer angular sur votre machine.

## Création d'un projet angular

Dans le terminal, se rendre dans l'endroit où l'on souhaite ranger le projet, puis `ng new nom-du-projet` pour initialiser un projet.
Choisir le type de style que l'on souhaite utiliser ( pour moi "CSS"), accepter le ssr avec -Y
Se rendre dans le projet avec la commande `cd nom-du-fichier`, l'ouvrir avec `code . -r` et lancer l'app avec la commande `ng serve --open`.
